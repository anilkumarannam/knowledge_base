#### 00A. General Commit
```
git add .
git commit -m "General Commit"
git push -u origin master

```

#### 001. Creating git Branch
```
git checkout -b ＜new-branch＞
```

#### 002. Delete git branch
```
git branch -d localBranchName
git push origin --delete remoteBranchName
```
#### 003. Creating React-Redux app
```
# Redux + Plain JS template
// Includes Redux tool kit
npx create-react-app my-app --template redux

# Redux + TypeScript template
npx create-react-app my-app --template redux-typescript
```

#### 004. Install a package
```
npm install <package-name>
```

#### 005. Adding Redux Provider component and store prop to it.
```
import React from 'react'
import ReactDOM from 'react-dom/client'

import { Provider } from 'react-redux'
import store from './store'

import App from './App'

// As of React 18
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <Provider store={store}>
    <App />
  </Provider>
)
```
#### 006. Redux Configuration.

**1. Install Redux Toolkit and React Redux**
```
npm install @reduxjs/toolkit react-redux
```
**2. Create a Redux Store at src/app/store.js**
```
import { configureStore } from '@reduxjs/toolkit'

export default configureStore({
  reducer: {},
})
```
**3. Provide the store to React**

Once the store is created, we can make it available to our React components by putting a React Redux Provider around our application in src/index.js. Import the Redux store we just created, put a Provider around your App, and pass the store as a prop.
```
import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import store from './app/store'
import { Provider } from 'react-redux'

// As of React 18
const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  <Provider store={store}>
    <App />
  </Provider>
)
```
**4. Create a Redux state slice**

Add a new file named src/features/counter/counterSlice.js. In that file, import the createSlice API from Redux Toolkit.
```
import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 0,
  },
  reducers: {
    increment: (state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.value += 1
    },
    decrement: (state) => {
      state.value -= 1
    },
    incrementByAmount: (state, action) => {
      state.value += action.payload
    },
  },
})

// Action creators are generated for each case reducer function
export const { increment, decrement, incrementByAmount } = counterSlice.actions

export default counterSlice.reducer
```
**5. Add Slice Reducer to the Store**

Next, we need to import the reducer function from the counter slice and add it to our store. By defining a field inside the reducers parameter, we tell the store to use this slice reducer function to handle all updates to that state.
```
import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../features/counter/counterSlice'

export default configureStore({
  reducer: {
    counter: counterReducer,
  },
})
```
**6. Use Redux State and Actions in React Components​**

Now we can use the React Redux hooks to let React components interact with the Redux store. We can read data from the store with useSelector, and dispatch actions using useDispatch. Create a src/features/counter/Counter.js file with a Counter component inside, then import that component into App.js and render it inside of App.
```
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './counterSlice'
import styles from './Counter.module.css'

export function Counter() {
  const count = useSelector((state) => state.counter.value)
  const dispatch = useDispatch()

  return (
    <div>
      <div>
        <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
        <span>{count}</span>
        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
      </div>
    </div>
  )
}
```
#### 07 Remove a directory.

```
rm -rf <dir>
```


###  OAuth Authentication Steps.